/*************************************************************************
 * Copyright (C) 1995-2000, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifdef __CINT__

#pragma link C++ class AtlasFieldSvc+;
#pragma link C++ class BFieldMeshZR+;
#pragma link C++ class H8FieldSvc+;
#pragma link C++ class BFieldH8Grid+;
#pragma link C++ class BFieldSolenoid+;
#pragma link C++ class components/MagFieldServices_entries+;

#endif

